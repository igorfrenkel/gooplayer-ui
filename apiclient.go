package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"

	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

type httpclient interface {
	Do(r *http.Request) (*http.Response, error)
}

type apiclient struct {
	http     httpclient
	hostname string
	lastresp *response
}

type response struct {
	err    error
	status status
}

type status struct {
	Track  track   `json:"track"`
	Volume volume  `json:"volume"`
	Tracks []track `json"alltracks,omitempty"`
}

type track string
type volume string

func newApiClient(hostname string) (*apiclient, error) {
	a := apiclient{http: &http.Client{}, hostname: hostname, lastresp: &response{}}
	return &a, nil
}

func (a *apiclient) status() {
	log.Debug("query /status")
	a.lastresp = a.query("GET", "status")
}

func (a *apiclient) prev() {
	a.lastresp = a.query("PUT", "prev")
}

func (a *apiclient) next() {
	a.lastresp = a.query("PUT", "next")
}

func (a *apiclient) volup() {
	a.lastresp = a.query("PUT", "volup")
}

func (a *apiclient) voldown() {
	a.lastresp = a.query("PUT", "voldown")
}

// TODO: no need for intermediary setters like next and prev
func (a *apiclient) query(method string, endpoint string) *response {
	url := fmt.Sprintf("%s/%s", a.hostname, endpoint)
	fmt.Println("query:", method, url)
	r := new(response)
	req, err := http.NewRequest(method, url, nil)
	if err != nil {
		r.err = errors.Wrap(err, "couldn't create new request")
		return r
	}
	resp, err := a.http.Do(req)
	if err != nil {
		r.err = errors.Wrap(err, "api (/status) get failed")
		return r
	}
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		r.err = errors.Wrap(err, "api (/status) response read failed")
		return r
	}
	var s status
	err = json.Unmarshal(body, &s)
	if err != nil {
		r.err = errors.Wrap(err, "api (/status) response fail to unmarshal json")
		return r
	}
	fmt.Println("query returned status", s)
	r.status = s
	return r
}

func (a *apiclient) err() error {
	if a.lastresp == nil || a.lastresp.err == nil {
		return nil
	}
	return a.lastresp.err
}

func (a *apiclient) trackname() string {
	if a.lastresp == nil {
		return "?"
	}
	res := strings.Replace(string(a.lastresp.status.Track), "-", " ", -1)
	res = strings.Replace(res, ".mp3", "", 1)
	return res
}

func (a *apiclient) volume() volume {
	if a.lastresp == nil {
		return volume("")
	}
	return a.lastresp.status.Volume
}

func (a *apiclient) tracknames() []string {
	if a.lastresp == nil {
		return nil
	}
	var res []string
	for _, t := range a.lastresp.status.Tracks {
		log.Info("t", t)
		tt := strings.Split(string(t), "/")
		if len(tt) < 1 {
			log.Warning("invalid track encountered", t)
			continue
		}
		s := tt[len(tt)-1]
		s = strings.Replace(s, "-", " ", -1)
		res = append(res, strings.Replace(s, ".mp3", "", 1))
	}
	log.Info("res", res)
	return res
}

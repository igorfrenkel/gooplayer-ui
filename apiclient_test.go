package main

import (
	"encoding/json"
	"io"
	"net/http"
	"strings"
	"testing"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/require"
)

type mockclient struct {
	mockdo func(req *http.Request) (*http.Response, error)
}

func (m *mockclient) Do(req *http.Request) (*http.Response, error) {
	return m.mockdo(req)
}

func TestQueryReq(t *testing.T) {
	a := apiclient{hostname: "http://foo.bar:9999"}
	m := new(mockclient)
	a.http = m
	called := false
	m.mockdo = func(req *http.Request) (*http.Response, error) {
		called = true
		require.Equal(t, "GET", req.Method)
		require.Equal(t, "http", req.URL.Scheme)
		require.Equal(t, "foo.bar:9999", req.URL.Host)
		require.Equal(t, "/baz", req.URL.Path)
		resp := new(http.Response)
		resp.Body = io.NopCloser(strings.NewReader("{}"))
		return resp, nil
	}
	a.query("GET", "baz")
	require.Equal(t, true, called)
}

func TestQueryResp(t *testing.T) {
	a := new(apiclient)
	m := new(mockclient)
	a.http = m

	st := status{
		Track:  "foo",
		Volume: "5",
		Tracks: []track{"foo", "bar", "baz"},
	}

	apiresp, err := json.Marshal(st)
	require.NoError(t, err)

	called := false
	m.mockdo = func(req *http.Request) (*http.Response, error) {
		called = true
		resp := new(http.Response)
		resp.Body = io.NopCloser(strings.NewReader(string(apiresp)))
		return resp, nil
	}
	r := a.query("GET", "http://foo.bar:9090/baz")
	require.Equal(t, true, called)
	require.Equal(t, st, r.status)
}

func TestQueryRespErr(t *testing.T) {
	a := new(apiclient)
	m := new(mockclient)
	a.http = m

	called := false
	e := errors.New("foo")
	m.mockdo = func(req *http.Request) (*http.Response, error) {
		called = true
		resp := new(http.Response)
		resp.Body = io.NopCloser(strings.NewReader(""))
		return resp, e
	}
	r := a.query("GET", "http://foo.bar:9090/baz")
	require.Equal(t, true, called)
	require.ErrorContains(t, r.err, e.Error())
}

func TestQueryTracks(t *testing.T) {
	a := new(apiclient)
	m := new(mockclient)
	a.http = m

	st := status{
		Tracks: []track{"foo", "bar", "baz"},
	}

	apiresp, err := json.Marshal(st)
	require.NoError(t, err)

	called := false
	m.mockdo = func(req *http.Request) (*http.Response, error) {
		called = true
		resp := new(http.Response)
		resp.Body = io.NopCloser(strings.NewReader(string(apiresp)))
		return resp, nil
	}
	r := a.query("GET", "http://foo.bar:9090/baz")
	require.Equal(t, true, called)
	require.Equal(t, st, r.status)
}

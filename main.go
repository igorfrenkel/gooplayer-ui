package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

const (
	title = "Goo Player"
)

func main() {
	router := gin.Default()
	router.LoadHTMLGlob("templates/*")

	cc, err := newApiClient("http://192.168.0.130:8900")
	log.Info("started api client")
	if err != nil {
		log.Error("api client err", err)
		return
	}
	router.GET("/reload4", func(c *gin.Context) {
		fmt.Println("get /reload")
		cc.status()
		c.Redirect(http.StatusFound, "/")
	})

	router.GET("/prev", func(c *gin.Context) {
		fmt.Println("get /prev")
		cc.prev()
		c.Redirect(http.StatusFound, "/")
	})

	router.GET("/next", func(c *gin.Context) {
		cc.next()
		c.Redirect(http.StatusFound, "/")
	})

	router.GET("/volup", func(c *gin.Context) {
		cc.volup()
		c.Redirect(http.StatusFound, "/")
	})

	router.GET("/voldown", func(c *gin.Context) {
		cc.voldown()
		c.Redirect(http.StatusFound, "/")
	})

	router.GET("/", func(c *gin.Context) {
		cc.status()
		c.HTML(http.StatusOK, "index.tmpl", gin.H{
			"title":  "Goo Player",
			"err":    cc.err(),
			"track":  cc.trackname(),
			"volume": cc.volume(),
			"tracks": cc.tracknames(),
		})
	})

	router.Run("192.168.0.130:8080")
}

func init() {
	log.SetLevel(log.DebugLevel)
}
